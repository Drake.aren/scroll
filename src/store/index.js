import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    article: null
  },
  mutations: {
    changeArticle (state, article) {
      state.article = article
    }
  }
})
