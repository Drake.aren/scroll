// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import vSelect from 'vue-select'
import Switches from 'vue-switches'
import VueMarkdown from 'vue-markdown'

Vue.component('v-icon', Icon)
Vue.component('v-select', vSelect)
Vue.component('v-switch', Switches)
Vue.component('v-markdown', VueMarkdown)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
