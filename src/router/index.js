import Vue from 'vue'
import Router from 'vue-router'
import ArticleList from '@/components/ArticleList'
import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/',
      name: 'Article List',
      component: ArticleList
    }
  ]
})
